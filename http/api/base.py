import json
from typing import Annotated
from fastapi import (
    APIRouter,
    HTTPException,
    Response,
    Body,
)
from sqlalchemy import exc, create_engine
from sqlalchemy.orm import Session
from utils.aux import gen_string, read_config
from models.dict import Urls

router = APIRouter()
base_url = read_config('URL_GENERATOR_ARGS', 'BASE_URL')
char_count = int(read_config('URL_GENERATOR_ARGS', 'CHAR_COUNT'))


def psql_engine():
    engine = create_engine("postgresql://"
        "{username}:{password}@{hostname}/{database}".format(
            hostname=read_config('NETWORK', 'PSQL_HOST'),
            port=read_config('NETWORK', 'PSQL_PORT'),
            database=read_config('DATABASE', 'DB_NAME'),
            username=read_config('DATABASE', 'PSQL_USER'),
            password=read_config('DATABASE', 'PSQL_PSWD'),
        ),
        pool_size = 20, max_overflow = 0
    )
    return engine

engine = psql_engine()

    
@router.post(
    '/create_short_url',
    tags=["url_operations"],
    description='adding new paire short+full urls',
)
async def create_short_url(
    data: Annotated[
        str,
        Body(
            example={
                "full_url": "<---adding url here---like: http://example.com/blabla--->"
            }
        )
    ]
    ) -> Response:
    try:
        full_url = json.loads(data)['full_url']
        response = await _create_short_url(full_url)
        return response
    except json.JSONDecodeError:
        raise HTTPException(status_code=400, detail='incorrect json input')


async def _create_short_url(full_url: str) -> Response:
    try:
        short_url = base_url + '/' + gen_string(char_count)
        with Session(engine) as session:
            existed = session.query(Urls).filter_by(full_url=full_url).first()
            if existed:
                # never create again
                return Response(status_code=200, content=existed.short_url)
            session.add(Urls(
                full_url = full_url,
                short_url = short_url,
                )
            )
            session.commit()
        return Response(status_code=200, content=short_url)
    except exc.SQLAlchemyError:
        raise HTTPException(status_code=503, detail='postgres server error')
    except Exception as e:
        raise HTTPException(status_code=500, detail=f'Internal server error: {e}')


@router.post(
    '/delete_short_url',
    tags=["url_operations"],
    description='remove url record from database',
)
async def delete_short_url(
    data: Annotated[
        str,
        Body(
            example={
                "short_url": "<---removing url here---like: const.com/<random.secrets>--->"
            }
        )
    ]
    ) -> Response:
    try:
        short_url = json.loads(data)['short_url']
        response = await _delete_short_url(short_url)
        return response
    except json.JSONDecodeError:
        raise HTTPException(status_code=400, detail='incorrect json input')


async def _delete_short_url(short_url: str) -> Response:
    try:
        with Session(engine) as session:
            existed = session.query(Urls).filter_by(short_url=short_url).first()
            if not existed:
                # nothing to delete
                return Response(status_code=200, content=short_url+' is not defined')
            session.delete(existed)
            session.commit()
        return Response(status_code=200, content=short_url+' was deleted')
    except exc.SQLAlchemyError:
        raise HTTPException(status_code=503, detail='postgres server error')
    except Exception as e:
        raise HTTPException(status_code=500, detail=f'Internal server error: {e}')


@router.get(
    '/get_full_url',
    tags=["url_operations"],
    description='denaming full url from short link',
)
async def get_full_url(
    data: Annotated[
        str,
        Body(
            example={
                "short_url": "<---denaming url here---like: const.com/<random.secrets>--->"
            }
        )
    ]
    ) -> Response:
    try:
        short_url = json.loads(data)['short_url']
        response = await _get_full_url(short_url)
        return response
    except json.JSONDecodeError:
        raise HTTPException(status_code=400, detail='incorrect json input')


async def _get_full_url(short_url: str) -> Response:
    try:
        with Session(engine) as session:
            existed = session.query(Urls).filter_by(short_url=short_url).first()
            if not existed:
                # nothing to return
                return Response(status_code=200, content=short_url+' is not defined')
        return Response(status_code=200, content=existed.full_url)
    except exc.SQLAlchemyError:
        raise HTTPException(status_code=503, detail='postgres server error')
    except Exception as e:
        raise HTTPException(status_code=500, detail=f'Internal server error: {e}')

