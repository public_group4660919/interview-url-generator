from datetime import datetime
from sqlalchemy import (
    Column,
    DateTime,
    text,
    Integer
)
from sqlalchemy.orm import declarative_base
from sqlalchemy_mixins import AllFeaturesMixin


Base = declarative_base(cls=AllFeaturesMixin)


class TimestampMixin:
    created_at = Column(
        DateTime,
        nullable=False,
        default=datetime.now,
        server_default=text("now()")
    )


class SerialMixin:
    id = Column(
        Integer, primary_key=True
    )


class SerialTimestampMixin(SerialMixin, TimestampMixin):
    pass
