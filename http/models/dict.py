# -*- coding: utf-8 -*-
from .base import Base, SerialTimestampMixin
from sqlalchemy import (
    Column,
    Integer,
    String,
)
from . import *

__all__ = ('Urls')


class Urls(Base, SerialTimestampMixin):
    __tablename__ = 'urls'
    __table_args__ = {'schema': 'routing'}

    id = Column(Integer, primary_key=True)
    full_url = Column('full_url', String(256), nullable=False)
    short_url = Column('short_url', String(256), nullable=False)


