import uvicorn
from fastapi import FastAPI
from api.base import router as base_router


class Config:
    DEFAULT_TIMEOUT = 20
    TAGS_METADATA = [
        {
            "name": "url_operations",
            "description": "Create, delete, select short urls"
        },
    ]
    BASIC_RESPONSES = {
        200: {
            'description': 'OK',
        },
        503: {
            'description': 'Postgres server error'
        },
        400: {
            'description': 'Input error: incorrect json'
        },
        500: {
            'description': 'Unknown server error'
        },
    }

app = FastAPI(
    openapi_tags=Config.TAGS_METADATA,
    docs_url="/docs",
    redoc_url=None,
    title="Interview_url_generator",
    version="dev",
    contact={
        "name": "no_one",
        "url": "https://nowhere.com/",
    },
)
app.include_router(base_router)


if __name__=="__main__":

    # FIXME: CREATE TEST DB SCHEMA
    from models.base import Base
    from api.base import engine
    from sqlalchemy.schema import CreateSchema
    schema = 'routing'
    if not engine.dialect.has_schema(engine, schema):
        engine.execute(CreateSchema(schema))
    Base.metadata.create_all(bind=engine)
    # FIXME: END

    uvicorn.run(app, host="0.0.0.0", port=8000)

