import secrets
import string
from configparser import ConfigParser


def read_config(section, key):
    config = ConfigParser()
    config.read('./utils/config.ini')
    return config[section][key]


def gen_string(length):
    return ''.join(secrets.choice(
        string.ascii_letters + string.digits        
        ) for i in range(length))

