# BUILD AND RUN PROJECT

- set your host IP to ./http/utils/config.ini
- execute:
    $ docker-compose --env-file ./http/utils/config.ini up -d
- open swagger and play with api's:
    [open swagger](http://localhost:8000/docs)


# SETTINGS

## available configuration parameters:

- network settings
- db settings
- basic url for short url generation

# FIXME

- for get up in one click "create (if not exist) schema, table into postgres db" was added in ./http/main.py
