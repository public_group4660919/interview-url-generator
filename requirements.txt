fastapi==0.95.1
uvicorn==0.22.0
SQLAlchemy==1.4.40
sqlalchemy_mixins==1.5.3
python-dotenv== 0.21.0
psycopg2-binary==2.9.5
psycopg2==2.9.5
